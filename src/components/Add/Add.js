import React, { Fragment, useState } from 'react';
import axios from 'axios';
import toastr from "toastr";
import "./Add.scss";

const Add = () => {
    const [datos, setDatos] = useState({
        nombre: '',
        cedula: '',
        fechaN: '',
        telefono: '',
    })

    const handleInputChange = (event) => {
        setDatos({
            ...datos,
            [event.target.name]: event.target.value
        })
    }

    const sleep = () => {
        setTimeout(() => window.location = `/`, 2000);
    }

    const handleSubmission = (event) => {
        event.preventDefault()
        const data = {
            name: datos.nombre,
            identification: datos.cedula,
            birth_date: datos.fechaN,
            phone_number: datos.telefono,
        }

        axios.post("http://127.0.0.1:8000/api/clients", data)
            .then((result) => {
                toastr.success('El cliente ha sido registrado con exito');
                sleep();
            }).catch((err) => {
                console.log(err);
                toastr.error('the server does not respond please try later');
            });

    }

    return (
        <Fragment>
            <div className="container container-app">
                <div className="row">
                    <div className="col">
                        <h1>Agregar Viajero</h1>
                    </div>
                </div>
                <form className="formApp" onSubmit={handleSubmission}>
                    <div className="col">
                        <input type="text" placeholder="Nombre" className="form-control" onChange={handleInputChange} name="nombre"></input>
                    </div>
                    <div className="col">
                        <input type="text" placeholder="Cédula" className="form-control" onChange={handleInputChange} name="cedula"></input>
                    </div>
                    <div className="col">
                        <input type="date" placeholder="Fecha de Nacimiento" className="form-control" onChange={handleInputChange} name="fechaN"></input>
                    </div>
                    <div className="col">
                        <input type="tel" placeholder="Teléfono" className="form-control" onChange={handleInputChange} name="telefono"></input>
                    </div>
                    <div className="col">
                        <button type="submit" className="btn btn-outline-primary">
                            Agregar
                        </button>
                    </div>
                </form>
            </div>
        </Fragment>
    );
}

export default Add;
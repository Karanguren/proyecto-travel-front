import React, { useEffect, useState } from 'react';
import axios from 'axios';
import "./List.scss";

function List() {
    const [allClients, setAllClients] = useState([]);
    
    useEffect(() => {
        const getAllClients = async () => {
            try {
                const data = await axios.get('http://127.0.0.1:8000/api/clients');
                setAllClients(data.data);
            } catch (error) {
                console.log(error.message);
            }
            console.log("Data", allClients);
        };
     
        getAllClients();
      }, []);


    return (
        <div className="container container-app">
            <div className="row">
                <div className="col">
                    <table className="table">
                        <thead>
                            <tr>
                                <th scope="col">Nombre</th>
                                <th scope="col">Options</th>
                            </tr>
                        </thead>
                        {allClients.map((client, index) => (
                            <tbody key={index}>
                                <tr>
                                    <th scope="row">{client.name}</th>
                                    <td>
                                        <a type="button" className="btn btn-outline-primary" href={`/view/${client.id}`}><i className="far fa-eye" /></a>
                                        <a type="button" className="btn btn-outline-success" href={`/edit/${client.id}`}><i className="far fa-edit" /></a>
                                        <a type="button" className="btn btn-outline-danger" href={`/delete/${client.id}`}><i className="fas fa-trash-alt" /></a>
                                    </td>
                                </tr>
                            </tbody>

                        ))}
                    </table>
                    <button
                        className="btn btn-outline-primary"
                        onClick={() => window.location = `/add`}
                        type="submit"
                    >
                        Agregar
                    </button>
                </div>
            </div>
        </div>
    );
}

export default List;

import React, { useEffect, Fragment, useState, useCallback } from 'react';
import axios from 'axios';
import { useParams } from 'react-router-dom'
import toastr from "toastr";
import "./Edit.scss";

const Edit = () => {
    const [Client, setClient] = useState([]);
    const [values, setValues] = useState({
        nombre: '',
        fechaN: '',
        cedula: '',
        telefono: '',
    });
    const { id } = useParams()
    
    useEffect(() => {
        axios.get(`http://127.0.0.1:8000/api/clients/${id}`).then((response) => {
            setClient(response.data)
        });
        console.log(Client)
    }, []);

    const handleInputChange = (event) => {
        setValues({
            ...values,
            [event.target.name]: event.target.value
        })
    }

    const sleep = () => {
        setTimeout(() => window.location = `/`, 2000);
    }

    const handleSubmission = (event) => {
        event.preventDefault()
        const data = {
            name: values.nombre,
            identification: values.cedula,
            birth_date: values.fechaN,
            phone_number: values.telefono,
        }
        console.log(data)

        // axios.put(`http://127.0.0.1:8000/api/clients/${id}`, data)
        //     .then((result) => {
        //         toastr.success('El cliente ha sido actualizado con exito');
        //         sleep();
        //     }).catch((err) => {
        //         console.log(err);
        //         toastr.error('the server does not respond please try later');
        //     });

    }

    return (
        <Fragment>
            <div className="container container-app">
                <div className="row">
                    <div className="col">
                        <h1>Editar Viajero</h1>
                    </div>
                </div>
                <form className="formApp" onSubmit={handleSubmission}>
                    <div className="col">
                        <input type="text" placeholder="Nombre" className="form-control" onChange={handleInputChange} name="nombre" value={Client.name}></input>
                    </div>
                    <div className="col">
                        <input type="text" placeholder="Cédula" className="form-control" onChange={handleInputChange} name="cedula" value={Client.identification}></input>
                    </div>
                    <div className="col">
                        <input type="date" placeholder="Fecha de Nacimiento" className="form-control" onChange={handleInputChange} name="fechaN" value={Client.birth_date}></input>
                    </div>
                    <div className="col">
                        <input type="tel" placeholder="Teléfono" className="form-control" onChange={handleInputChange} name="telefono" value={Client.phone_number}></input>
                    </div>
                    <div className="col">
                        <button type="submit" className="btn btn-outline-primary">
                            Actualizar
                        </button>
                    </div>
                </form>
            </div>
        </Fragment>
    );
}

export default Edit;
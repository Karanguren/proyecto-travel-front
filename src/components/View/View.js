import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useParams } from 'react-router-dom'
import "./View.scss";

function View() {
    const [Clients, setClients] = useState([]);
    const { id } = useParams()
    
    useEffect(() => {
        axios.get(`http://127.0.0.1:8000/api/clients/${id}`)
            .then((response) =>
            {setClients(response.data)}
            );
            console.log(Clients)
    }, []);

	return (
        <div className="container container-app">
            <div className="row">
                <div className="col view">
                    <h2>{Clients.name}</h2>
                    <p>Cédula</p>
                    <h4>{Clients.identification}</h4>
                    <p>Fecha de Nacimiento</p>
                    <h4>{Clients.birth_date}</h4>
                    <p>Teléfono</p>
                    <h4>{Clients.phone_number}</h4>
                    <p>Viajes</p>
                    {/* <ul>
                    <li>{datos.nombre}</li>
                    <li>{datos.cedula}</li>
                    <li>{datos.fechaN}</li>
                    <li>{datos.telefono}</li>
                    </ul> */}
                </div>
            </div>
                <div className="col">
                    <button
                        className="btn btn-outline-dark"
                        onClick={() => window.location='/'}
                        type="submit"
                    >
                        Volver
                    </button>
                </div>
        </div>
    )
}

export default View;

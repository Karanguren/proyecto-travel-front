import React from 'react';
import axios from 'axios';
import toastr from "toastr";
import { useParams } from 'react-router-dom'
import Question from "../../assets/img/Question.png";
import "./Delete.scss";

function Delete() {
    const { id } = useParams()

    const sleep = () => {
        setTimeout(() => window.location = `/`, 2000);
    }

    const handleDelete = () => {

        axios.delete(`http://127.0.0.1:8000/api/clients/${id}`)
            .then((result) => {
                toastr.success('El cliente ha sido eliminado con exito');
                sleep();
            }).catch((err) => {
                console.log(err);
                toastr.error('the server does not respond please try later');
            });

    }

	return (
        <div className="container container-app">
            <div className="row">
                <div className="col">
                    <img src={Question} className="check" alt="logo" />
                    <p className="font">¿Esta seguro que desea eliminar al viajero?</p>
                    <button
                        className="btn btn-outline-dark button"
                        onClick={handleDelete}
                        type="submit"
                    >
                        Eliminar
                    </button>
                    <button
                        className="btn btn-outline-dark button"
                        onClick={() => window.location='/'}
                        type="submit"
                    >
                        Volver
                    </button>
                </div>
            </div>
        </div>
    )
}

export default Delete;

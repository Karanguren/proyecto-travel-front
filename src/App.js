import Routers from "./Router/Router";
import Navbar from "./components/Navbar/Navbar";
import "../node_modules/toastr/toastr.scss";
import './App.css';

function App() {
  return (
    
    <div className="App">
        <Navbar/>
        <Routers/>
    </div>
  );
}

export default App;

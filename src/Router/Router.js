import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";

import List from "../components/List/List";
import Add from "../components/Add/Add";
import Edit from "../components/Edit/Edit";
import Delete from "../components/Delete/Delete";
import View from "../components/View/View";


class Routers extends Component {
    
	render() {

		return (
			<Switch>
				<Route exact path="/" component={List} />
				<Route exact path="/add" component={Add} />
				<Route exact path="/edit/:id" component={Edit} />
				<Route exact path="/delete/:id" component={Delete} />
				<Route exact path="/view/:id" component={View} />
			</Switch>
		);
	}
}

export default Routers;